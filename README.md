# planetary-system-api

API REST desarrollada en Node.JS para el ml-challenge.

### About

- API REST escrita en TypeScript. Diseñada para trabajar con MongoDB. Base de datos alojada en [atlas](https://www.mongodb.com/cloud/atlas).
- Documentación desarrollada con [swagger](https://www.npmjs.com/package/swagger-ui-express)
- Seguridad implementada con [auth0](https://auth0.com/)
- Test realizados con [mochajs](https://mochajs.org/) y [chaijs](https://www.chaijs.com/)

### Supuestos

- Esta API solo consulta los datos generados por el job [planetary-orbit-calculator](https://gitlab.com/mercado-libre-challenge/planetary-orbit-calculator)
- Se realizaron test con covertura total pero "superficial"; es decir, no se terminaron de probar todas las posibilidades de si para X no puede pasar Y, etc.
- Los test se realizan sobre una base de datos igual pero no la misma que la "productiva". Productiva: beer_db, Test: beer_db_test.
- No usar la base de tests como persistencia ya que se borran todos los datos en cada run de test.

### Features

Podemos consultar:

- ¿Cuántos períodos de sequía habrá?
- ¿Cuántos períodos de lluvia habrá y qué día será el pico máximo de lluvia?
- ¿Cuántos períodos de condiciones óptimas de presión y temperatura habrá?

### Install

```
git clone https://gitlab.com/mercado-libre-challenge/api
npm install
```

### Run

```
npm run webpack
```

in other bash session:

```
npm start
```

Checkear las variables de entorno

### Testing

To run the tests:

```
npm test
```

### Help

- [Swagger docs](http://localhost:3000/api-docs)
- [Requests de postman](https://gitlab.com/mercado-libre-challenge/api/blob/master/ml-challenge.postman_collection.json)

### Author

- [Rodrigo Calvo](https://gitlab.com/recalvo)
- [Consultas](mailto:rodrigocalvo95@gmail.com)
