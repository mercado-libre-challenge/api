/**
 * Required External Modules
 */

import * as dotenv from "dotenv";
import express, { Request, Response } from "express";
import cors from "cors";
import helmet from "helmet";

const swaggerUi: any = require("swagger-ui-express");
import * as swaggerDocument from "./swagger.json";

/**
 * Required Internal Modules
 */
import { jwtCheck, createToken } from "./middleware/security";
import { errorHandler } from "./middleware/error.middleware";
import MongoDB from "./common/dbconnector";

import { systemRouter } from "./system/system.router";
import { statisticRouter } from "./statistic/statistic.router";
import { _Res } from "./common/response.class";

dotenv.config();

/**
 * Variables
 */

if (!process.env.PORT) {
  process.exit(1);
}

const PORT: number = parseInt(process.env.PORT as string, 10);

const app: any = express();
const mongoDB: MongoDB = new MongoDB();

/**
 *  App
 */
mongoDB.connect();

app.use(helmet());
app.use(cors());
app.use(express.json());

app.get("/insecured", async (req: Request, res: Response) => {
  res.status(200).send("No secured Resource");
});

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get("/token", async (req: Request, res: Response) => {
  try {
    const token: any = await createToken();
    res.status(200).send(token);
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

/* Seguridad */
app.use(jwtCheck);

app.get("/secured", async (req: Request, res: Response) => {
  res.status(200).send("Secured Resource");
});

app.use("/system", systemRouter);
app.use("/statistic", statisticRouter);

app.use(errorHandler);


/**
 * Server
 */

const server: any = app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});

/**
 * Webpack HMR
 */

type ModuleId = string | number;

interface IWebpackHotModule {
  hot?: {
    data: any;
    accept(
      dependencies: string[],
      callback?: (updatedDependencies: ModuleId[]) => void
    ): void;
    accept(dependency: string, callback?: () => void): void;
    accept(errHandler?: (err: Error) => void): void;
    dispose(callback: (data: any) => void): void;
  };
}

declare const module: IWebpackHotModule;

if (module.hot) {
  module.hot.accept();
  module.hot.dispose(() => server.close());
}
