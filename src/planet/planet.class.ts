import { prop, getModelForClass } from "@typegoose/typegoose";

export class Planet {
  @prop({ required: true, trim: true, unique: true })
  public name?: string;

  @prop({ required: true, default: 0 })
  public angular_velocity?: number;

  @prop({ required: true, default: 0 })
  public star_distance?: number;

  constructor(data?: any) {
    if (data) {
      this.name = data.name || "";
      this.angular_velocity = data.angular_velocity || 0;
      this.star_distance = data.star_distance || 0;
    }
  }
}

export const PlanetModel: any = getModelForClass(Planet);
