/**
 * Data Model Interfaces
 */

import { System, SystemModel } from "./system.class";
import { Weather } from "./weather.class";
import { Cache } from "./../common/cache.service";
import { Planet } from "../planet/planet.class";

const cache: Cache<System> = new Cache<System>();

const populate: any = {
  path: "orbits.planet",
  model: Planet
};
/**
 * Service Methods
 */

export default class SystemClass {
  public findByDay = async (day: number): Promise<System> => {
    let record: System = cache.get(day.toString());

    if (!record) {
      record = await SystemModel.findOne({ day: day }).populate(populate);

      if (record) {
        cache.put(day.toString(), record);
      }
    }

    if (record) {
      return record;
    }

    throw new Error("No record found");
  }

  public findByDays = async (days: number[]): Promise<System[]> => {
    return await SystemModel.find({ day: { $in: days } }).populate(populate);
  }

  public getByWeather = async (weather: Weather): Promise<Array<System>> => {
    return await SystemModel.find({ weather: weather }).populate(populate);
  }
}