
export enum Weather {
    /**
     * Cuando los tres planetas están alineados entre sí y a su vez alineados con respecto al sol, el
      sistema solar experimenta un período de sequía.
     */
    dirt = "dirt",

    /**
     * Cuando los tres planetas no están alineados, forman entre sí un triángulo. Es sabido que en el
      momento en el que el sol se encuentra dentro del triángulo, el sistema solar experimenta un
      período de lluvia, teniendo éste, un pico de intensidad cuando el perímetro del triángulo está en
      su máximo.
     */
    rain = "rain",
    max_rain = "max_rain",

    /**
     * Las condiciones óptimas de presión y temperatura se dan cuando los tres planetas están
      alineados entre sí pero no están alineados con el sol.
     */
    fine = "fine",
}