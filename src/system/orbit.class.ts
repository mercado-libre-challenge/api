import { prop, Ref } from "@typegoose/typegoose";
import { Planet } from "./../planet/planet.class";
import mongoose from "mongoose";

export class Orbit {
    @prop({ ref: "Planet", refType: mongoose.Schema.Types.ObjectId, required: true })
    public planet?: Ref<Planet>;

    @prop({ required: true, default: 0 })
    public position?: number;

    constructor(data?: any) {
        if (data) {
            this.planet = data!.planet._id || data!.planet || null;
            this.position = data!.position || 0;
        }
    }
}