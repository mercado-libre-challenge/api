/**
 * Required External Modules and Interfaces
 */
import express, { Request, Response } from "express";
import SystemService from "./system.service";
import StatisticService from "./../statistic/statistic.service";
import { _Res } from "../common/response.class";
import { System } from "./system.class";
import { Weather } from "./weather.class";
import { Statistic } from "../statistic/statistic.class";

/**
 * Service Definition
 */
const systemService: SystemService = new SystemService();
const statisticService: StatisticService = new StatisticService();

/**
 * Router Definition
 */
export const systemRouter = express.Router();

/**
 * Controller Definitions
 */

// GET system/:day
systemRouter.get("/max-rain", async (req: Request, res: Response) => {
    try {
        const statistic: Statistic = await statisticService.get();

        if (!statistic) {
            throw new Error("Not found")
        }

        const systems: System[] = await systemService.findByDays(statistic.days_max_rain!);

        res.status(200).send(systems);

    } catch (e) {
        res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
    }
});

// GET system/:weather
systemRouter.get("/weather/:weather", async (req: Request, res: Response) => {
    let weather: Weather = Weather.dirt;

    switch (req.params.weather) {
        case "fine":
            weather = Weather.fine;
            break;
        case "dirt":
            weather = Weather.dirt;
            break;
        case "max_rain":
            weather = Weather.max_rain;
            break;
        case "rain":
            weather = Weather.rain;
            break;
        default:
            weather = Weather.dirt;
            break;
    }

    try {
        const systems: System[] = await systemService.getByWeather(weather);
        res.status(200).send(systems);
    } catch (e) {
        res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
    }
});


// GET system/:day
systemRouter.get("/:day", async (req: Request, res: Response) => {
    const day: number = parseInt(req.params.day);

    try {
        const system: System = await systemService.findByDay(day);
        res.status(200).send(system);
    } catch (e) {
        res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
    }
});