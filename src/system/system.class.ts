import { index, prop, getModelForClass, Ref } from "@typegoose/typegoose";
import { Orbit } from "./orbit.class";
import { Weather } from "./weather.class";

@index({ day: 1 }, { unique: true })
export class System {
  @prop({ required: true, default: 0 })
  public day?: number;

  @prop()
  public orbits?: Array<Orbit>;

  @prop({ required: true })
  public weather?: Weather;
}

export const SystemModel: any = getModelForClass(System);
