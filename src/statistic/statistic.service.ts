/**
 * Data Model Interfaces
 */

import { Statistic, StatisticModel } from "./statistic.class";
import { Cache } from "../common/cache.service";

/**
 * Service Methods
 */
const cache: Cache<Statistic> = new Cache<Statistic>();

export default class StatisticClass {
    public get = async (): Promise<Statistic> => {
        let record: Statistic = cache.get("statistic");

        if (!record) {
            record = await StatisticModel.findOne();
            cache.put("statistic", record);
        }

        if (record) {
            return record;
        }

        throw new Error("No record found");
    }
}