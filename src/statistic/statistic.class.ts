import { prop, getModelForClass } from "@typegoose/typegoose";

export class Statistic {
    @prop({ required: true, default: 0 })
    public dirty_periods_count?: number;

    @prop({ required: true, default: 0 })
    public fine_periods_count?: number;

    @prop({ required: true, default: 0 })
    public rain_periods_count?: number;

    @prop({ required: true, default: 0 })
    public max_rain_periods_count?: number;

    @prop({ required: true, default: 0 })
    public days_max_rain?: Array<number>;

    constructor(data?: any) {
        this.dirty_periods_count = data.dirty_periods_count || 0;
        this.fine_periods_count = data.fine_periods_count || 0;
        this.rain_periods_count = data.rain_periods_count || 0;
        this.max_rain_periods_count = data.max_rain_periods_count || 0;
        this.days_max_rain = data.days_max_rain || [];
    }
}

export const StatisticModel: any = getModelForClass(Statistic);
