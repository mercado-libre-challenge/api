/**
 * Required External Modules and Interfaces
 */
import express, { Request, Response } from "express";
import StatisticService from "./statistic.service";
import { _Res } from "../common/response.class";
import { Statistic } from "./statistic.class";

/**
 * Service Definition
 */
const statisticService: StatisticService = new StatisticService();

/**
 * Router Definition
 */
export const statisticRouter = express.Router();

/**
 * Controller Definitions
 */

// GET statistic/
statisticRouter.get("/", async (req: Request, res: Response) => {
    try {
        const statistic: Statistic = await statisticService.get();
        res.status(200).send(statistic);
    } catch (e) {
        res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
    }
});
