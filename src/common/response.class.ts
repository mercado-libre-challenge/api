
import { Email } from "./email.service";
import * as EmailService from "./email.service";

export class _Res {
  public status?: number;
  public message?: string;
  private request?: string;
  private body?: string;


  public toJSON(): any {
    return {
      status: this.status,
      message: this.message
    };
  }

  async log(response: _Res): Promise<void> {
    await EmailService.sendEmail(
      new Email({
        to: process.env.ERROR_EMAIL as string,
        subject: "Error santander_challenge_beer",
        html: `Date: ${new Date()}<br/>Req: ${response.request}<br/>Body: ${response.body}<br/>Error message: ${response.message}`
      })
    );
  }

  constructor(data: any) {
    this.status = data.status || 200;
    this.message = data.message || "";
    this.request = data.request || null;
    this.body = data.request || null;

    if (this.status === 500) {
      this.log(this);
    }
  }
}
