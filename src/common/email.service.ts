import * as dotenv from "dotenv";
dotenv.config();

const sgMail: any = require("@sendgrid/mail");

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);

const APP_EMAIL: string = process.env.APP_EMAIL as string;

export class Email {
    public to?: string;
    public from?: string;
    public subject?: string;
    public html?: string;

    constructor(data: any) {
        this.to = data.to || APP_EMAIL;
        this.from = data.from || APP_EMAIL;
        this.subject = data.subject || "";
        this.html = data.html || "";
    }
}

/**
 * Service Methods
 */

export const sendEmail: any = async (email: Email): Promise<Email> => {
    try {
        if (process.env.SENDGRID_API_KEY as string) {
            const result: any = await sgMail.send(email);
            return result;
        } else {
            return new Email({});
        }
    } catch (e) {
        throw e;
    }
};