const mongoose = require("mongoose");

export default class MongoDB {
    public async connect(): Promise<void> {
        const DB_URL: String = process.env.DATABASE_CONNECTION_STRING as string;

        if (!DB_URL) {
            throw new Error("process.env.DATABASE_CONNECTION_STRING is empty");
        }

        mongoose.connect(
            DB_URL,
            { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false, useUnifiedTopology: true }
        );

        const IS_PRODUCTION: boolean = process.env.IS_PRODUCTION as string === "1";
        const IS_DEBUG_ENABLED: boolean = process.env.IS_DEBUG_ENABLED as string === "1";

        if (!IS_PRODUCTION && IS_DEBUG_ENABLED) {
            console.log("debug true");
            mongoose.set("debug", true);
        }

        const db = mongoose.connection;
        db.on("error",
            console.error.bind(console, "connection error:")
        );
        db.once("open", () => {
            console.log("DB conectada -> " + DB_URL);
        });
    }
}
/*
export const database_connect = async () => {
    const DB_URL: String = process.env.DATABASE_CONNECTION_STRING as string;

    if (!DB_URL) {
        throw new Error("process.env.DATABASE_CONNECTION_STRING is empty")
    }

    mongoose.connect(
        DB_URL,
        { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false }
    );

    const IS_PRODUCTION: boolean = process.env.IS_PRODUCTION as string === "1";
    const IS_DEBUG_ENABLED: boolean = process.env.IS_DEBUG_ENABLED as string === "1";

    if (!IS_PRODUCTION && IS_DEBUG_ENABLED) {
        console.log("debug true");
        mongoose.set("debug", true);
    }

    const db = mongoose.connection;
    db.on("error",
        console.error.bind(console, "connection error:")
    );
    db.once("open", () => {
        console.log("DB conectada -> " + DB_URL);
    });
};
*/