export class Cache<T> {
    private values: Map<string, T> = new Map<string, T>();
    private maxEntries: number = 100;

    public get(key: string): T {
        const hasKey: boolean = this.values.has(key);
        let entry: T;
        if (hasKey) {
            // peek the entry, re-insert for LRU strategy
            entry = this.values.get(key)!;
            this.values.delete(key);
            this.values.set(key, entry);
        }

        return entry!;
    }

    public put(key: string, value: T): void {
        if (this.values.has(key)) {
            this.values.delete(key);
        }
        if (this.values.size >= this.maxEntries) {
            // least-recently used cache eviction strategy
            const keyToDelete: any = this.values.keys().next().value;

            this.values.delete(keyToDelete);
        }
        this.values.set(key, value);
    }

    public delete(key: string): void {
        if (this.values.has(key)) {
            this.values.delete(key);
        }
    }
}