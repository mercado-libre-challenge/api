import jwt from "express-jwt";
import jwksRsa from "jwks-rsa";
import * as dotenv from "dotenv";

dotenv.config();

const AUTH0_DOMAIN: string = process.env.AUTH0_DOMAIN as string;
const AUTH0_AUDIENCE: string = process.env.AUTH0_AUDIENCE as string;

export const jwtCheck: any = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: AUTH0_DOMAIN + ".well-known/jwks.json"
    }),
    audience: AUTH0_AUDIENCE,
    issuer: AUTH0_DOMAIN,
    algorithms: ["RS256"]
}).unless({
    path: [
        "/user/login",
        "/user/create"
    ]
});

export const createToken: any = async (): Promise<any> => {
    return new Promise<any>((resolve, reject) => {
        const unirest: any = require("unirest");
        const AUTH0_DOMAIN: string = process.env.AUTH0_DOMAIN as string;
        const AUTH0_AUDIENCE: string = process.env.AUTH0_AUDIENCE as string;
        const AUTH0_CLIENT_ID: string = process.env.AUTH0_CLIENT_ID as string;
        const AUTH0_CLIENT_SECRET: string = process.env.AUTH0_CLIENT_SECRET as string;

        const req: any = unirest(
            "POST",
            `${AUTH0_DOMAIN}oauth/token`
        );

        req.headers({
            "content-type": "application/json"
        });

        req.send({
            client_id: AUTH0_CLIENT_ID,
            client_secret: AUTH0_CLIENT_SECRET,
            audience: AUTH0_AUDIENCE,
            grant_type: "client_credentials"
        });

        req.end(
            async (res: any): Promise<any> => {
                if (res.error) {
                    reject(res.error);
                }

                resolve(res.body);
            }
        );
    });
};