FROM node:12.2.0-alpine as build

# Create app directory
WORKDIR /usr/src/app

ENV TZ='America/Argentina/Buenos_Aires'
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && apk add --no-cache tzdata

ENV NODE_ENV=production

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# If you are building your code for production
RUN npm ci --only=production

# Bundle app source
COPY . .


EXPOSE 3000
CMD [ "npm", "start" ]
