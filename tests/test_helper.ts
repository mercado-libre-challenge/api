import * as dotenv from "dotenv";
dotenv.config();

const mongoose = require("mongoose");
// tell mongoose to use es6 implementation of promises
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DATABASE_TEST_CONNECTION_STRING as string, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
});
mongoose.connection
  // tslint:disable-next-line: no-empty
  .once("open", () => {})
  .on("error", (error: any) => {
    console.warn("Error : ", error);
  });

after(done => {
  done();
});
