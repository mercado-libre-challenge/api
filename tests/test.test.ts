import { expect } from "chai";
import SystemService from "./../src/system/system.service";
import StatisticService from "./../src/statistic/statistic.service";
import { System } from "./../src/system/system.class";
import { Weather } from "../src/system/weather.class";
import { Statistic } from "../src/statistic/statistic.class";


const systemService: SystemService = new SystemService();
const statisticService: StatisticService = new StatisticService();

describe("Planets API", () => {
    before((done) => {
        require("./test_helper");
        done();
    });
    describe("System", () => {
        it("findByDay", (done) => {
            systemService.findByDay(9).then((system: System) => {
                expect(system.day).to.equal(9);
                expect(system.weather).to.equal("rain");
                expect(system.orbits![2].position).to.equal(270);

                done();
            });
        });
        it("findByDays", (done) => {
            systemService.findByDays([4, 5, 6]).then((systems: System[]) => {
                expect(systems[0].day).to.equal(4);
                expect(systems[0].weather).to.equal("max_rain");
                expect(systems[0].orbits![0].position).to.equal(40);

                expect(systems[1].day).to.equal(5);
                expect(systems[1].weather).to.equal("max_rain");
                expect(systems[1].orbits![1].position).to.equal(100);

                expect(systems[2].day).to.equal(6);
                expect(systems[2].weather).to.equal("max_rain");
                expect(systems[2].orbits![2].position).to.equal(180);

                done();
            });
        });
        it("getByWeather", (done) => {
            systemService.getByWeather(Weather.rain).then((systems: System[]) => {
                expect(systems.length).to.equal(2);

                expect(systems[0].day).to.equal(9);
                expect(systems[0].weather).to.equal("rain");
                expect(systems[0].orbits![0].position).to.equal(90);
                expect(systems[0].orbits![1].position).to.equal(180);
                expect(systems[0].orbits![2].position).to.equal(270);

                expect(systems[1].day).to.equal(10);
                expect(systems[1].weather).to.equal("rain");
                expect(systems[1].orbits![0].position).to.equal(100);
                expect(systems[1].orbits![1].position).to.equal(200);
                expect(systems[1].orbits![2].position).to.equal(300);

                done();
            });
        });
    });
    describe("Statistic", () => {
        it("get", (done) => {
            statisticService.get().then((statistic: Statistic) => {
                expect(statistic.dirty_periods_count).to.equal(1);
                expect(statistic.fine_periods_count).to.equal(0);
                expect(statistic.rain_periods_count).to.equal(2);
                expect(statistic.max_rain_periods_count).to.equal(8);
                expect(statistic.days_max_rain?.length).to.equal(1);
                expect(statistic.days_max_rain![0]).to.equal(8);

                done();
            });
        });
    });
});